#! /bin/sh

if [ -z "$OPENROUTESERVICEKEY" ]
then
      echo "\$OPENROUTESERVICEKEY is empty"
else
    ./build-data.py 
    ./build-site.sh 
    docker-compose up -d
fi



