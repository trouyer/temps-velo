#! /usr/bin/python3

import overpy

api = overpy.Overpass()

result = api.query("""
    [out:json][timeout:25];
    nwr["place"]["place"!~"square|locality"](area:3600905682);
    out body;
    """)

    

f = open("points.csv", "w")

f.write('level;lat;lng;query;name\n')
for node in result.nodes:
    if node.tags['place'] == 'city' or node.tags['place'] == 'town' or node.tags['place'] == 'village':
        f.write('10;' + str(node.lat) + ';' + str(node.lon) + ';;' + node.tags['name'] + '\n')
    if node.tags['place'] == 'suburb':
        f.write('15;' + str(node.lat) + ';' + str(node.lon) + ';;' + node.tags['name'] + '\n')

f.close()
